
import java.util.Scanner;

public class xo {
	static int row,col;
    static char[][] table;
    static char currentPlayer = 'X';
    static Scanner kb = new Scanner(System.in);

    public static void createTable() {
        table = new char[3][3];
        for (int rows = 0; rows < 3; rows++) {
            for (int cols = 0; cols < 3; cols++) {
                table[rows][cols] = '-';
            }
        }
    }

    public static void printTable() {

        for (int rows = 0; rows < 3; rows++) {
            for (int cols = 0; cols < 3; cols++) {
                System.out.print(table[rows][cols] + " ");
            }
            System.out.println();
        }
    }

    public static void printTitle() {
        System.out.println("************************");
        System.out.println("Welcome to XO");
        System.out.println("************************");
    }

    public static void printPlayerTurn() {
        System.out.println(currentPlayer + "'s Turn");
    }

    public static boolean WinRow() {
        for (int rows = 0; rows < 3; rows++) {
            if (table[row][0] == currentPlayer && table[row][1] == currentPlayer && table[row][2] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    public static boolean WinCol() {
        for (int cols = 0; cols < 3; cols++) {
            if (table[0][col] == currentPlayer && table[1][col] == currentPlayer && table[2][col] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    public static boolean WinCross() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer ;
           
    }

    public static boolean Win() {
        return (WinRow() || WinCol() || WinCross()) ? true : false;

    }

    public static boolean Draw() {
        for (int rows = 0; rows < 3; rows++) {
            for (int cols = 0; cols < 3; cols++) {
                if (table[rows][cols] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean nextround() {
        String choice;
        while (true) {
            System.out.print("please input continue(y/n): ");
            choice = kb.next().toLowerCase();
            if (choice.equals("n")) {
                return false;
            } else if (choice.equals("y")) {
                break;
            } else {
                System.out.println("Please enter y or n");
            }
        }
        return true;
    }

    public static boolean OutOfBound(int row, int col) {
        return row < 0 || row >= 3 || col < 0 || col >= 3;
    }

    public static boolean NotEmpty(int row, int col) {
        return table[row][col] != '-';
    }

    public static void insertRC() {
        System.out.print("Please row and col (1-3): ");
        row = kb.nextInt() - 1;
        col = kb.nextInt() - 1;
    }

   

    public static boolean hasEnd() {
        if (Win()) {
            System.out.println(currentPlayer + " wins!");
            return true;
        } else if (Draw()) {
            System.out.println("The game ends in a draw");
            return true;
        }
        return false;
    }

    public static boolean InvalidMove(int row, int col) {
        if (OutOfBound(row, col) || NotEmpty(row, col)) {
            System.out.println("Invalid move. Try again.");
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        createTable();
        while (true) {
            printTitle();
            printTable();
            while (true){
                printPlayerTurn();
                insertRC();
                if (InvalidMove(row, col)) continue;
                table[row][col] = currentPlayer;
                printTable();
                if (hasEnd()) break;
                currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
            }
            if (!nextround()) break;
        }
    }
}
